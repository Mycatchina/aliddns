[![star](https://gitee.com/Mycatchina/aliddns/badge/star.svg?theme=dark)](https://gitee.com/Mycatchina/aliddns/stargazers)
## Aliddns 使用 Python3 编写
**需要有阿里云域名，自建DDNS，适用于没有公网IP的服务器**
##### 码云项目地址
```
git clone https://gitee.com/Mycatchina/aliddns.git
```


```
# 安装依赖
pip3 install pyyaml
pip3 install aliyun-python-sdk-core-v3
pip3 install aliyun-python-sdk-alidns==2.0.6
```

##### 参考链接 <https://gitee.com/wang_li/AutoUpdateAliDnsRecord>